package week3_4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

public class ServiceTest {
    private Service service;

    @BeforeEach
    public void init() {
        service = new Service();
    }

    @Test
    public void testGetMode() {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 7, 7, 8, 9, 10);
        Assertions.assertEquals(7, service.getMode(list));
    }

    @Test
    public void testGetMean() {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 7, 7, 8, 9, 10);
        Assertions.assertEquals(5.75, service.getMean(list));
    }

    @Test
    public void testGetMedian() {
        List<Integer> list = new java.util.ArrayList<>(List.of(1, 2, 3, 4, 5, 6, 7, 7, 7, 8, 9, 10));
        Collections.sort(list);
        Assertions.assertEquals(6.5, service.getMedian(list));
    }
}
