package week2_4.animal;

public abstract class Animal {
    protected int numberOfLegs;
    protected String favoriteFood;

    public abstract int getNumberOfLegs();
    public abstract String getFavoriteFood();
}
