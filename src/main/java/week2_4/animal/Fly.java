package week2_4.animal;

public class Fly extends Animal {
    public Fly(int numberOfLegs, String favoriteFood) {
        super.numberOfLegs = numberOfLegs;
        super.favoriteFood = favoriteFood;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return favoriteFood;
    }
}
