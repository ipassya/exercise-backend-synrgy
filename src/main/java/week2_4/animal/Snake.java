package week2_4.animal;

public class Snake extends Animal {
    public Snake(int numberOfLegs, String favoriteFood) {
        super.numberOfLegs = numberOfLegs;
        super.favoriteFood = favoriteFood;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return favoriteFood;
    }
}
