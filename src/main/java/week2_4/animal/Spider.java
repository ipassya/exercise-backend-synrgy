package week2_4.animal;

public class Spider extends Animal {
    public Spider(int numberOfLegs, String favoriteFood) {
        super.numberOfLegs = numberOfLegs;
        super.favoriteFood = favoriteFood;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String getFavoriteFood() {
        return favoriteFood;
    }
}
