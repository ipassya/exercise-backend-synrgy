package week2_4.animal;

public class App {
    public static void main(String[] args) {
        Animal[] animals = new Animal[6];

        animals[0] = new Dog(4, "Daging");
        animals[1] = new Spider(8, "Serangga");
        animals[2] = new Fly(0, "Kotoran");
        animals[3] = new Centipede(100, "Daun");
        animals[4] = new Snake(0, "Tikus");
        animals[5] = new Chicken(2, "Kacang");

        for (int i = 0; i < animals.length; i++) {
            System.out.printf("----- %s -----\n", animals[i].getClass().getSimpleName());
            System.out.printf("Jumlah kaki: %d\n", animals[i].getNumberOfLegs());
            System.out.printf("Makanan Favorit: %s\n", animals[i].getFavoriteFood());
            System.out.println();
        }
    }
}
