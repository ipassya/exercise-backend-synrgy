package week2_4.finance;

public class App {
    public static void main(String[] args) {
        Account account = new Account("03425", "Putra Hakim");
        Account savingsAccount = new SavingsAccount("03425", "Putra Hakim", 250.45);

        System.out.println("Hasil dari toString() untuk Account adalah:");
        System.out.println(account);
        System.out.println("\nHasil dari toString() untuk SavingsAccount adalah:");
        System.out.println(savingsAccount);
    }
}
