package week2_4.finance;

public class Account {
    private String name;
    private String accountNumber;

    public Account(String accountNumber, String name) {
        this.name = name;
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return accountNumber + "\n" + name;
    }
}
