package week2_4.finance;

public class SavingsAccount extends Account {
    private double balance;

    public SavingsAccount(String accountNumber,String name,  double balance) {
        super(accountNumber, name);
        this.balance = balance;
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public void withdraw(double amount) {
        balance -= amount;
    }

    @Override
    public String toString() {
        return getAccountNumber() + "\n" + getName() + "\n$" + balance;
    }
}
