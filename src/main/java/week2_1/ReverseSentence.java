package week2_1;

import java.util.Scanner;

public class ReverseSentence {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan kalimat: ");
        String sentence = input.nextLine();
        input.close();
        String result = "";

        for (int i = sentence.length() - 1; i >= 0; i--) {
            result += sentence.charAt(i);
        }

        System.out.println("Kalimat terbalik = " + result);
    }
}
