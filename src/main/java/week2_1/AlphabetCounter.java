package week2_1;

import java.util.Scanner;

public class AlphabetCounter {
    public static char[] alphabet = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    public static int[] result = new int[alphabet.length];

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("variable = ");
        String variable = input.nextLine().toLowerCase();
        input.close();

        for (int i = 0; i < variable.length(); i++) {
            alphabetChecker(variable.charAt(i));
        }

        results();
    }

    private static void alphabetChecker (char variable) {
        for (int i = 0; i < alphabet.length; i++) {
            if (variable == alphabet[i]) {
                result[i]++;
            }
        }
    }

    private static void results() {
        for (int i = 0; i < result.length; i++) {
            if (result[i] != 0) {
                System.out.printf("Alfabet %s muncul %d kali\n", alphabet[i], result[i]);
            }
        }
    }
}
