package week3_4;

import java.io.*;
import java.util.*;

public class App {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String path = "C:/Users/ihzap/OneDrive/Documents/GitLab/exercise-backend-synrgy/src/main/java/week3_4/data/";
        File schoolData = new File(path + "data_sekolah.csv");
        BufferedReader reader;
        PrintWriter writer;
        int selected;

        do {
            selected = getOption(path, scanner, schoolData);
            switch (selected) {
                case 0:
                    exit();
                    break;
                case 1:
                    reader = new BufferedReader(new FileReader(schoolData));
                    writer = new PrintWriter(new FileWriter(path + "data_sekolah_modus.txt"));
                    generateModus(reader, writer);
                    reader.close();
                    writer.close();

                    selected = getOptionAfterGenerate(path, scanner);
                    break;
                case 2:
                    reader = new BufferedReader(new FileReader(schoolData));
                    writer = new PrintWriter(new FileWriter(path + "data_sekolah_mean_median.txt"));
                    generateMeanAndMedian(reader, writer);
                    reader.close();
                    writer.close();

                    selected = getOptionAfterGenerate(path, scanner);
                    break;
                case 3:
                    reader = new BufferedReader(new FileReader(schoolData));
                    writer = new PrintWriter(new FileWriter(path + "data_sekolah_modus.txt"));
                    generateModus(reader, writer);
                    reader.close();
                    writer.close();

                    reader = new BufferedReader(new FileReader(schoolData));
                    writer = new PrintWriter(new FileWriter(path + "data_sekolah_mean_median.txt"));
                    generateMeanAndMedian(reader, writer);
                    reader.close();
                    writer.close();

                    selected = getOptionAfterGenerate(path, scanner);
                    break;
                default:
                    System.out.println("\n**Masukkan pilihan dengan angka [0-3]**\n");
                    break;
            }
        } while (selected != 0);
    }

    public static void appName() {
        System.out.println("\n\n--------------------------------------------------------------------");
        System.out.println("|                   Aplikasi Pengolah Nilai Siswa                  |");
        System.out.println("--------------------------------------------------------------------");
    }

    public static int getOption(String path, Scanner scanner, File file) {
        boolean isInteger = false;
        int select = 0;

        while (!isInteger) {
            try {
                appName();
                System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut: ");
                System.out.println(path + "\n");

                System.out.println("Menu");
                System.out.println("1. Generate txt untuk menampilkan modus");
                System.out.println("2. Generate txt untuk menampilkan rata-rata, median");
                System.out.println("3. Generate kedua file");
                System.out.println("0. Exit");
                System.out.print("Pilih menu [0-3]: ");
                select = scanner.nextInt();

                isInteger = true;
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("\n**Masukkan pilihan dengan angka [0-3]**\n");
            }
        }

        if (!file.isFile()) {
            isInteger = false;
            while (!isInteger) {
                try {
                    appName();
                    System.out.println("File tidak ditemukan\n");
                    System.out.println("0. Exit");
                    System.out.println("1. Kembali ke menu utama");
                    System.out.print("Pilih [0-1]: ");
                    select = scanner.nextInt();

                    isInteger = true;
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                    System.out.println("\n**Masukkan pilihan dengan angka [0-1]**\n");
                }
            }

            if (select == 1) {
                return getOption(path, scanner, file);
            }
        }

        return select;
    }

    public static int getOptionAfterGenerate(String path, Scanner scanner) {
        boolean isInteger = false;
        int select = 0;

        while (!isInteger) {
            try {
                appName();
                System.out.println("File telah di generate di: ");
                System.out.println(path);
                System.out.println("silahkan cek\n");
                System.out.println("0. Exit");
                System.out.println("1. Kembali ke menu utama");
                System.out.print("Pilih [0-1]: ");
                select = scanner.nextInt();

                isInteger = true;
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("\n**Masukkan pilihan dengan angka [0-1]**\n");
            }
        }

        while (select > 1) {
            isInteger = false;
            while (!isInteger) {
                try {
                    System.out.println("\n**Masukkan pilihan dengan angka [0-1]**\n");
                    System.out.println("0. Exit");
                    System.out.println("1. Kembali ke menu utama");
                    System.out.print("Pilih [0-1]: ");
                    select = scanner.nextInt();

                    isInteger = true;
                } catch (InputMismatchException e) {
                    scanner.nextLine();
                }
            }
        }

        if (select == 0) {
            exit();
        }

        return select;
    }

    public static void exit() {
        System.out.println("\nTerima kasih telah menggunakan Aplikasi Pengolah Nilai Siswa :)");
    }

    public static List<Integer> readFile(BufferedReader reader) throws IOException {
        List<Integer> records = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            String[] values = line.split(";");
            for (int i = 1; i < values.length; i++) {
                records.add(Integer.parseInt(values[i]));
            }
        }
        reader.close();

        return records;
    }

    public static void generateModus(BufferedReader reader, PrintWriter writer) throws IOException {
        Service service = new Service();
        List<Integer> data = readFile(reader);

        writer.println("Berikut hasil sebaran data nilai");
        writer.println("Modus: " + service.getMode(data));
        writer.close();
    }

    public static void generateMeanAndMedian(BufferedReader reader, PrintWriter writer) throws IOException {
        Service service = new Service();
        List<Integer> data = readFile(reader);

        writer.println("Berikut hasil sebaran data nilai");
        writer.println("Mean: " + service.getMean(data));
        writer.println("Median: " + service.getMedian(data));
        writer.close();
    }
}
