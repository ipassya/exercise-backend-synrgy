package week3_4;

import java.util.Collections;
import java.util.List;

public class Service {
    public int getMode(List<Integer> data) {
        int mode = 0;
        int max = 0;
        int temp, tempCount;

        for (int i = 0; i < data.size(); i++) {
            temp = data.get(i);
            tempCount = 0;
            for (Integer record : data) {
                if (temp == record) {
                    tempCount++;
                }
            }
            if (tempCount > max) {
                max = tempCount;
                mode = temp;
            }
        }

        return mode;
    }

    public double getMean(List<Integer> data) {
        double mean;
        double sum = 0;

        for (Integer record : data) {
            sum += record;
        }

        mean = sum / data.size();

        return mean;
    }

    public double getMedian(List<Integer> data) {
        double median = 0;
        int size = data.size();
        int mid = size / 2;

        Collections.sort(data);

        if (size % 2 == 0) {
            median = (double) (data.get(mid) + data.get(mid - 1)) / 2;
        } else {
            median = data.get(mid);
        }

        return median;
    }
}
