package week3_2;

import java.util.InputMismatchException;
import java.util.Scanner;

// BMI Application
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double weight, height, bmi;
        boolean isNumber = false;

        while (!isNumber) {
            try {
                System.out.print("Masukkan berat badan (kg): ");
                weight = scanner.nextDouble();
                System.out.print("Masukkan tinggi badan (cm): ");
                height = scanner.nextDouble();

                bmi = weight / Math.pow((height / 100), 2);

                if (bmi > 30) {
                    System.out.println("Anda obesitas");
                } else if (bmi > 25) {
                    System.out.println("Anda kelebihan berat badan");
                } else if (bmi > 18.5) {
                    System.out.println("Anda sehat");
                } else {
                    System.out.println("Anda kekurangan berat badan");
                }

                isNumber = true;
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Masukkan input yang sesuai");
            }
        }
        scanner.close();
    }
}
